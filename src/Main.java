import it.uniroma2.pjdm.figures.*;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		String tokens[] = line.split(" ");
		
		for(String tok : tokens) {
			Figure f;
			if(tok.startsWith("c")) {
				f = new Figure() {
					public double computeArea() {
						return Math.PI * super.getSize() * super.getSize();
					}
					public double computePerimeter() {
						return Math.PI * 2.0 * super.getSize();
					}
				};
			} else if(tok.startsWith("s")) {
				f = new Figure() {
					public double computeArea() {
						return super.getSize() * super.getSize();
					}
					public double computePerimeter() {
						return 4.0 * super.getSize();
					}
				};
			} else {
				continue;
			}
			String s = tok.substring(1);
			try {
				f.setSize(s);
			} catch (NotADoubleException e) {
				System.out.println("invalid number");
				continue;
			}
			System.out.println(f.getClass().getSimpleName() + " " + f + " " + f.computeArea() + " " + f.computePerimeter());
		}
		
		scanner.close();
	}

}
