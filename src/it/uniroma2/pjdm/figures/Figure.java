package it.uniroma2.pjdm.figures;

public abstract class Figure {
	private double size;
	
	public void setSize(double size) {
		this.size = size;
	}
	
	public double getSize() {
		return this.size;
	}
	
	public void setSize(String ssize) throws NotADoubleException {
		try {
			this.size = Double.parseDouble(ssize);
		} catch (NumberFormatException e) {
			throw new NotADoubleException();
		}
	}
	
	public abstract double computeArea();
	
	public abstract double computePerimeter();
	
	public String toString() {
		return "" + this.size;
	}
}
